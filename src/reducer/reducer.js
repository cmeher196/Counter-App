const count={
  count:0
}
export function reducer(state=count,action){
  switch(action.type){
  case 'INCREMENT':
    return {count:state.count+1}
  case 'DECREMENT':
    return {count:state.count-1}
  default:
     return state;
  }
}
