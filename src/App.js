import React from 'react';
import {connect} from  'react-redux';


class App extends React.Component{

  increment= () =>{
    this.props.dispatch({type:'INCREMENT'});
  }

  decrement =() =>{
    this.props.dispatch({type:'DECREMENT'});
  }
render(){
  console.log('this.props:',this.props);
  return(
    <div>
    <h2>Counter App Using Redux</h2>
    <div>
    <button onClick={this.decrement}>-</button>
    <span>Counter:{this.props.count}</span>
    <button onClick={this.increment}>+</button>
    </div>
    </div>
  );
}
}
function mapStateToProps(state){
  return{
    count:state.count
  };
}



export default connect(mapStateToProps)(App);
//export default App;
